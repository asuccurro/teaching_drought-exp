# DAY 3

# RMA

esetRMA.col0wd = rma(col0wd)
sampleNames(esetRMA.col0wd)
head(featureNames(esetRMA.col0wd))
length(featureNames(esetRMA.col0wd))

# Make boxplots of raw and pre-processed data
boxplot(col0wd)
boxplot(esetRMA.col0wd)

MAplot(col0wd)
MAplot(esetRMA.col0wd)

# Probes to Genes
col0wd
library(ath1121501.db)
ath1121501.db
# Get the environment list
ath1121501()
# Select a probe id and have a look at some environment
x  = "245035_at"
ath1121501GENENAME[[x]]
ath1121501ENZYME[[x]]
ath1121501SYMBOL[[x]]
ath1121501ACCNUM[[x]]

# Genes of interest in van Dijk et al.
papergenes = c("AT4G34000", "AT5G51990", "AT4G27410", "AT5G52310", "AT5G52300", "AT2G46680", "AT3G61890")

# Obtain the probe id corresponding to papergenes[[1]]
accnum2probe = as.data.frame(ath1121501ACCNUM)
findprobe = function(gid, df){
  return(df$probe_id[df$gene_id==gid])
}
paperprobes = sapply(papergenes, findprobe, accnum2probe)

# qPCR genes
qpcrgenes = c("AT1G52690", "AT3G06420", "AT5G36910", "AT2G14750", "AT4G04610", "AT1G74090")
qpcrprobes = sapply(qpcrgenes, findprobe, accnum2probe)


# Obtain the mean expr values for each sample group
# make sure you know your sample groups
phenoData(col0wd)$type
pd = phenoData(col0wd)
# get expr values
# Now, for each of these probes we want to obtain a vector of expression value means of the sample groups (in our case, one value for Watered, one for Drought, for each probe)

expval.col0wd = exprs(esetRMA.col0wd)
expval.col0wd[1:10,]
e.col0w = expval.col0wd[paperprobes, pd$type == "W"]
e.col0d = expval.col0wd[paperprobes, pd$type == "D"]

myttest = function(pb, df1, df2){
  return(wilcox.test(df1[pb,], df2[pb,]))
}

# Use limma
# phenoData(col0wd)$type is already a factor object
design = model.matrix(~ 0 + phenoData(col0wd)$type)
colnames(design) = c("Drought", "Watered")
dfit = lmFit(expval.col0wd, design)
# contrast matrix
cm = makeContrasts(Drought-Watered,levels=design)
dfit.con = contrasts.fit(dfit, cm)
# Compute moderated t-test on each probe; check t-statistics and p-values
dfit.eb = eBayes(dfit.con)
dfit.eb$coefficients[1:10,]
dfit.eb$t[1:10,]
dfit.eb$p.value[1:10,]

name = "Volcano.jpg"
jpeg(name)
volcanoplot(dfit.eb,coef=1,highlight=20)
dev.off()

install.packages(c("ggplot2", "ggrepel", "dplyr"))
library("ggplot2") #Best plots
library("ggrepel") #Avoid overlapping labels
library("dplyr")

df = as.data.frame(dfit.eb)
results = mutate(df, sig=ifelse(df$p.value<0.005, "p value<0.05", "Not Sig"))
g=rownames(dfit.eb$coefficients)
gn=sapply(g, findprobe, accnum2probe)
results = mutate(results, gene=gn)
p = ggplot(results, aes(coefficients, -log10(p.value))) +
  geom_point(aes(col=sig)) +
  scale_color_manual(values=c("red", "black"))
p
p+geom_text(data=filter(results, p.value<0.005), aes(label=gene))


# correct for multiple testing
genetab.col0wd = topTable(dfit.eb,coef=1,number=length(dfit.eb),adjust.method="BH")
siggenetab.col0wd = genetab.col0wd[genetab.col0wd$adj.P.Val < 0.001, ]
upregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC > 1,]
downregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC < -1,]
dim(downregtab.col0wd)
dim(upregtab.col0wd)

# Plot a heatmap of the selected genes
sev = expval.col0wd[rownames(siggenetab.col0wd), ]
heatmap(sev)

# Get list of probes differentialy expressed deprobs.list
#deprobs.symbol = unlist( mget(deprobs.list, envir = ath1121501SYMBOL) )

