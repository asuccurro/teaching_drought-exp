# Day 4 - expression values analysis on all samples

############# 4.a get the data

source('~/repositories/gitlab/teaching_drought-exp/r_scripts/setups_and_auxiliary_functions.r')
source('~/repositories/gitlab/teaching_drought-exp/r_scripts/load_and_qc.r')
col0wd = loadAndFormatAffyData("data/", "GSM290", "data/dataVanDijk.csv", "Watered or Drought")
esetRMA.col0wd = affy::rma(col0wd)


source('~/repositories/gitlab/teaching_drought-exp/r_scripts/get_col0ron1.r')
source('~/repositories/gitlab/teaching_drought-exp/r_scripts/get_col0alx8.r')

############# 4.b get DE for W D

source('~/repositories/gitlab/teaching_drought-exp/r_scripts/get_differential_expressions.r')

design.wd = model.matrix(~ 0 + phenoData(col0wd)$type)
design.wd
colnames(design.wd) = c("Test", "Control")
design.wd

eb.col0wd = get_differential_expressions(exprs(esetRMA.col0wd), design.wd)
fullDE.col0wd = topTable(eb.col0wd,coef=1,number=dim(eb.col0wd)[1],adjust.method="BH")

jpeg("outputs/volcano_wd.jpg")
volcanoplot(eb.col0wd,coef=1,highlight=50)
dev.off()


############# 4.c get DE for WT RON1

design.ron1 = model.matrix(~ 0 + phenoData(lerron1)$type)
design.ron1
colnames(design.ron1) = c("Test", "Control")
design.ron1

eb.lerron1 = get_differential_expressions(exprs(esetRMA.lerron1), design.ron1)
fullDE.lerron1 = topTable(eb.lerron1,coef=1,number=dim(eb.lerron1)[1],adjust.method="BH")

jpeg("outputs/volcano_ron1.jpg")
volcanoplot(eb.lerron1,coef=1,highlight=20)
dev.off()



############# 4.d get DE for WT ALX8

design.alx8 = model.matrix(~ 0 + phenoData(col0alx8)$type)
design.alx8
colnames(design.alx8) = c("Test", "C24", "fry1", "Control")
design.alx8

eb.col0alx8 = get_differential_expressions(exprs(esetRMA.col0alx8), design.alx8)
fullDE.col0alx8 = topTable(eb.col0alx8,coef=1,number=dim(eb.col0alx8)[1],adjust.method="BH")

jpeg("outputs/volcano_alx8.jpg")
volcanoplot(eb.col0alx8,coef=1,highlight=20)
dev.off()


design.fry1 = model.matrix(~ 0 + phenoData(col0alx8)$type)
design.fry1
colnames(design.fry1) = c("alx8", "Control", "Test", "wt")
design.fry1

eb.c24fry1 = get_differential_expressions(exprs(esetRMA.col0alx8), design.fry1)
fullDE.c24fry1 = topTable(eb.c24fry1,coef=1,number=dim(eb.c24fry1)[1],adjust.method="BH")

jpeg("outputs/volcano_fry1.jpg")
volcanoplot(eb.c24fry1,coef=1,highlight=20)
dev.off()

############# 4.e Check genes of interest

# Obtain the probe id corresponding to papergenes[[1]]
accnum2probe = as.data.frame(ath1121501ACCNUM)
# Genes of interest in van Dijk et al.
papergenes = c("AT4G34000", "AT5G51990", "AT4G27410", "AT5G52310", "AT5G52300", "AT2G46680", "AT3G61890")
paperprobes = sapply(papergenes, findprobe, accnum2probe)
# qPCR genes
qpcrgenes = c("AT1G52690", "AT3G50980", "AT3G06420", "AT2G37640", "AT5G36910", "AT2G14750", "AT4G04610", "AT2G20610", "AT3G45140", "AT1G74090")
qpcrprobes = sapply(qpcrgenes, findprobe, accnum2probe)

print_gene_table = function(sList, sNames, gList, cName, oDir){
  df = data.frame(matrix(0, length(gList), length(sNames)))
  colnames(df) = sNames
  rownames(df) = gList
  for (i in 1:length(sNames)){
    df[,i] = sList[[i]][gList,cName]
  }
  write.csv(df, paste(oDir, "_", cName, ".csv", sep=""))
}

print_gene_table(list(fullDE.col0wd, fullDE.lerron1, fullDE.col0alx8, fullDE.c24fry1), c("drought", "ron1", "alx8", "fry1"), 
                 paperprobes, "logFC", "outputs/papergenes")

print_gene_table(list(fullDE.col0wd, fullDE.lerron1, fullDE.col0alx8, fullDE.c24fry1), c("drought", "ron1", "alx8", "fry1"), 
                 paperprobes, "adj.P.Val", "outputs/papergenes")

print_gene_table(list(fullDE.col0wd, fullDE.col0ron1, fullDE.col0alx8, fullDE.c24fry1), c("drought", "ron1", "alx8", "fry1"), 
                 qpcrprobes, "logFC", "outputs/qpcrgenes")

print_gene_table(list(fullDE.col0wd, fullDE.col0ron1, fullDE.col0alx8, fullDE.c24fry1), c("drought", "ron1", "alx8", "fry1"), 
                 qpcrprobes, "adj.P.Val", "outputs/qpcrgenes")



#########

myFDR = 0.005

library(dplyr)
gtron1 = as.data.frame(fullDE.lerron1$adj.P.Val)
gtron1 = mutate(gtron1, lerron1=ifelse(gtron1$`fullDE.lerron1$adj.P.Val`<myFDR, TRUE, FALSE))
rownames(gtron1) =rownames(fullDE.lerron1)

gtALX8 = as.data.frame(fullDE.col0alx8$adj.P.Val)
gtALX8 = mutate(gtALX8, col0alx8=ifelse(gtALX8$`fullDE.col0alx8$adj.P.Val`<myFDR, TRUE, FALSE))
rownames(gtALX8) =rownames(fullDE.col0alx8)

gtwd = as.data.frame(fullDE.col0wd$adj.P.Val)
gtwd = mutate(gtwd, col0wd=ifelse(gtwd$`fullDE.col0wd$adj.P.Val`<myFDR, TRUE, FALSE))
rownames(gtwd) =rownames(fullDE.col0wd)

sgdf = data.frame(col0wd=gtwd[accnum2probe$probe_id, "col0wd"], col0alx8=gtALX8[accnum2probe$probe_id, "col0alx8"], 
                  lerron1=gtron1[accnum2probe$probe_id, "lerron1"])
rownames(sgdf) = accnum2probe$probe_id

library(gplots)
venn(sgdf)


x = sgdf[sgdf$col0wd == TRUE & sgdf$col0alx8 == TRUE & sgdf$lerron1 == TRUE,]

x = sgdf[sgdf$col0wd == TRUE & sgdf$col0alx8 == TRUE,]
dim(x)

px = rownames(x)
px

ewd = exprs(esetRMA.col0wd)
eron = exprs(esetRMA.lerron1)
ealx = exprs(esetRMA.col0alx8)

em = cbind(ewd[px,], eron[px,], ealx[px,])
jpeg("outputs/heatmap_allsamples.jpg")
heatmap(em)
dev.off()


gtfry1 = as.data.frame(fullDE.c24fry1$adj.P.Val)
gtfry1 = mutate(gtfry1, c24fry1=ifelse(gtfry1$`fullDE.c24fry1$adj.P.Val`<myFDR, TRUE, FALSE))
rownames(gtfry1) =rownames(fullDE.c24fry1)


sgdf_4 = data.frame(col0wd=gtwd[accnum2probe$probe_id, "col0wd"], col0alx8=gtALX8[accnum2probe$probe_id, "col0alx8"], 
                  lerron1=gtron1[accnum2probe$probe_id, "lerron1"], c24fry1=gtfry1[accnum2probe$probe_id, "c24fry1"])
rownames(sgdf_4) = accnum2probe$probe_id

venn(sgdf_4)


sigfullDE.col0wd = fullDE.col0wd[fullDE.col0wd$adj.P.Val < myFDR, ]
upregtab.col0wd = sigfullDE.col0wd[sigfullDE.col0wd$logFC > 1,]
downregtab.col0wd = sigfullDE.col0wd[sigfullDE.col0wd$logFC < -1,]
dim(downregtab.col0wd)
dim(upregtab.col0wd)

# Plot a heatmap of the selected genes
sev = exprs(esetRMA.col0wd)[rownames(sigfullDE.col0wd), ]
hmcol = colorRampPalette(rev(brewer.pal(9, "PuOr")))(255)
jpeg("outputs/heatmap_wd.jpg")
heatmap(sev, col = rev(hmcol))
dev.off()












x = sgdf_4[sgdf_4$col0wd == TRUE & sgdf_4$c24fry1 == TRUE,]
dim(x)


px = rownames(x)
px






wd = fullDE.col0wd[px, "logFC"]
alx8 = fullDE.col0alx8[px, "logFC"]
ron1 = fullDE.lerron1[px, "logFC"]
fry1 = fullDE.c24fry1[px, "logFC"]
lfm = cbind(wd, alx8, ron1, fry1)

heatmap(lfm, col = rev(hmcol))


