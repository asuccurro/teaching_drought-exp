
loadAndFormatAffyData = function(dirName, fNamePat, fGroups, expCond, passingFileList = FALSE){
  # Function to load and format .CEL files (raw affymetrix files)
  # dirName = relative path to the folder where the files are
  # fNamePat = file name pattern to select only the .CEL files of interest
  # fGroups = file name (including relative path) for the .csv file that classifies the files to the sample groups
  # expCond = experimental conditions, e.g. "Wild Type or ron1 mutant"
  
  # load the affy package
  library(affy)
  
  # get the list of .CEL files
  if (passingFileList){
    flist = fNamePat
  } else {
    flist = list.celfiles(dirName, full.names=TRUE, pattern=fNamePat)
  }
  
  # create the AffyBatch object
  myAffyBatch = ReadAffy(filenames = flist)
  
  # load the table to classify the sample groups
  filetab = read.csv(fGroups)
  
  # rename rows with the file name ("file" field)
  rownames(filetab) = filetab$file

  # by default, ReadAffy assigns the file names as sample names
  # remove the .CEL string from the sample name to then match filetab
  samplenames = sampleNames(myAffyBatch)
  sampleNames(myAffyBatch) = sub("\\.CEL$", "", samplenames)
  samplenames = sampleNames(myAffyBatch)

  # add phenoData
  
  # in case the order of file names and sample names is different, obtain the vector of index
  mti = match(rownames(filetab), sampleNames(myAffyBatch))
  
  # create a meta data of data.frame type
  metad <- data.frame(labelDescription = c(paste("Experimental condition: ", expCond), "Sample ID", "Replicate ID"))
  phenoData(myAffyBatch) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)
  
  return(myAffyBatch)
}

doQCAndRMAOnAffyData = function(myAffyBatch, outDirName){
  # Function to perform some basic QC on the myAffyBatch, storing outputs in outDirName
  # Returns an ExpressionSet obtained with rma method
  # myAffyBatch = AffyBatch object
  # outDirName = output directory
  library(simpleaffy)
  library(affy)
  library(affyPLM)
  
  # get a vector of the sample names, to use for naming the output files
  snames = sampleNames(myAffyBatch)
  
  # call the qc method from simpleaffy
  myQC = qc(myAffyBatch)
  
  # write the table of percent present genes check on a csv file
  write.csv(myQC@percent.present, paste(outDirName, cdfName(myAffyBatch), "_qc_percent_present.csv", sep=""))
  
  # plot the qc check on 3' to 5' ratios
  png(paste(outDirName, cdfName(myAffyBatch), "qc_ratios.png", sep=""))
  plot(myQC)
  dev.off()
  
  # get the expression set
  myESet = rma(myAffyBatch)
  
  # loop over the samples of the AffyBatch and save the QC plots
  for (i in 1:length(snames)){
    n0 = paste(outDirName, snames[i], "_", sep="")
    png(paste(n0,"affymetrix.png", sep=""))
    image(myAffyBatch[,i])
    dev.off()
    png(paste(n0,"raw_MAplot.png", sep=""))
    MAplot(myAffyBatch, which=i)
    dev.off()
    png(paste(n0,"norm_MAplot.png", sep=""))
    MAplot(myESet, which=i)
    dev.off()
  }
  return(myESet)
}

