# DAY 5 work with GO

biocLite("GO.db")
biocLite("GOFunction")
library(GO.db)
library(annotate)
library(GOFunction)
biocLite("cogena")
library(cogena)

# Get the GO terms for the siggenes
siggenes = rownames(siggenetab)
siggoids = c()
for (g in siggenes[1:10]){
  for (t in ath1121501GO[[g]]){
    siggoids = c(siggoids, t$GOID)
  }
}
siggoids = unique(siggoids)
siggoprobes = ath1121501GO2PROBE[siggoids]


aracyc = as.data.frame(ath1121501ARACYC)
degenes = rownames(genetab[genetab$adj.P.Val < 0.001, ])
ndegenes = rownames(genetab[genetab$adj.P.Val >= 0.001, ])
paths = unique(aracyc$pathway_name)
morethanonefunctions = c()
# initialize the data frame
conttab = data.frame(row.names = c(paths, "NA"))
l = length(paths)
conttab$deg = l*c(0)
conttab$ndeg = l*c(0)
countpaths = function(gl, df, dfcol, db, mf) {
  for (g in gl) {
    pts = unique(db[db$probe_id==g, "pathway_name"])
    if (length(pts) < 1){
      df["NA", dfcol] = df["NA", dfcol] + 1
    }
    if (length(pts) > 1){
      #print(g)
      mf = c(mf, g)
    }
    for (p in pts) {
      df[p, dfcol] = df[p, dfcol] + 1
    }
  }
  res = list(df, mf)
  return(res)
}
x = countpaths(degenes, conttab, "deg", aracyc, morethanonefunctions)
conttab = x[[1]]
morethanonefunctions = x[[2]]
x = countpaths(ndegenes, conttab, "ndeg", aracyc, morethanonefunctions)
conttab = x[[1]]
morethanonefunctions = x[[2]]
sum(conttab$deg)


conttab$tot = conttab$deg + conttab$ndeg
