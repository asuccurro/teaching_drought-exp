library(gplots)
f = read.csv("data/E-TABM-566.sdrf.txt", sep = '\t')
colnames(f)
f$Factor.Value..genotype.
f$Sample.Name
f$Array.Data.File
fl = paste("data/", f$Array.Data.File, sep="")
col0ron1 = ReadAffy(filenames = fl)

tf = f[, c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
colnames(tf) = colnames(filetab)
tf$file = sub("\\.CEL", "", tf$file)
rownames(tf) = tf$file
tf$type = sub("wild type", "wt", tf$type)
tf$type = sub("ron1-1", "ron1", tf$type)

d2 = function(affyobj, filetab, expcond){
  samplenames = sampleNames(affyobj)
  sampleNames(affyobj) = sub("\\.CEL$", "", samplenames)
  samplenames = sampleNames(affyobj)
  # add phenoData
  mti = match(rownames(filetab), sampleNames(affyobj))
  metad <- data.frame(labelDescription = c(paste("Experimental condition: ", expcond), "Sample ID", "Replicate ID"))
  phenoData(affyobj) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)
  # inspect the phenoData
  print(varMetadata(phenoData(affyobj)))
  print(phenoData(affyobj)$type)
  return(affyobj)
}

col0ron1 = d2(col0ron1, tf, "Wild Type or ron1 mutant")

## QC 
QC.col0ron1 = qc(col0ron1)
plot(QC.col0ron1)

## Preproc

esetRMA.col0ron1 = rma(col0ron1)
sampleNames(esetRMA.col0ron1)
head(featureNames(esetRMA.col0ron1))
length(featureNames(esetRMA.col0ron1))

# Make boxplots of raw and pre-processed data
boxplot(col0ron1)
#boxplot(esetRMA.col0ron1)

## Compute expr
expval.col0ron1 = exprs(esetRMA.col0ron1)
design = model.matrix(~ 0 + phenoData(col0ron1)$type)
colnames(design) = c("Mutant", "WildType")
dfit2 = lmFit(expval.col0ron1, design)
# contrast matrix
cm = makeContrasts(Mutant-WildType,levels=design)
dfit2.con = contrasts.fit(dfit2, cm)
# Compute moderated t-test on each probe; check t-statistics and p-values
dfit2.eb = eBayes(dfit2.con)
dfit2.eb$coefficients[1:10,]
dfit2.eb$t[1:10,]
dfit2.eb$p.value[1:10,]
name = "VolcanoRon1WT.jpg"
jpeg(name)
volcanoplot(dfit2.eb,coef=1,highlight=20)
dev.off()

# correct for multiple testing
genetab.col0ron1 = topTable(dfit2.eb,coef=1,number=length(dfit2.eb),adjust.method="BH")
siggenetab.col0ron1 = genetab.col0ron1[genetab.col0ron1$adj.P.Val < 0.001, ]
upregtab.col0ron1 = siggenetab.col0ron1[siggenetab.col0ron1$logFC > 1,]
downregtab.col0ron1 = siggenetab.col0ron1[siggenetab.col0ron1$logFC < -1,]
dim(downregtab.col0ron1)
dim(upregtab.col0ron1)
# Plot a heatmap of the selected genes
sev = expval.col0ron1[rownames(siggenetab.col0ron1), ]
heatmap(sev)

gtron1 = as.data.frame(genetab.col0ron1$adj.P.Val)
gtron1 = mutate(gtron1, col0ron1=ifelse(gtron1$`genetab.col0ron1$adj.P.Val`<0.001, TRUE, FALSE))
rownames(gtron1) =rownames(genetab.col0ron1)

gtALX8 = as.data.frame(genetab.col0alx8$adj.P.Val)
gtALX8 = mutate(gtALX8, col0alx8=ifelse(gtALX8$`genetab.col0alx8$adj.P.Val`<0.001, TRUE, FALSE))
rownames(gtALX8) =rownames(genetab.col0alx8)

gtwd = as.data.frame(genetab.col0wd$adj.P.Val)
gtwd = mutate(gtwd, col0wd=ifelse(gtwd$`genetab.col0wd$adj.P.Val`<0.001, TRUE, FALSE))
rownames(gtwd) =rownames(genetab.col0wd)

sgdf = data.frame(col0wd=gtwd[accnum2probe$probe_id, "col0wd"], col0alx8=gtALX8[accnum2probe$probe_id, "col0alx8"], col0ron1=gtron1[accnum2probe$probe_id, "col0ron1"])
rownames(sgdf) = accnum2probe$probe_id

library(gplots)
venn(sgdf)

allin = rownames(sgdf[(sgdf$col0wd==TRUE) & (sgdf$col0alx8==TRUE) & (sgdf$col0ron1==TRUE),])

unlist(as.list(ath1121501ACCNUM[allin,]))
