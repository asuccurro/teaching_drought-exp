library(limma)
library(affy)

library(dplyr)


setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")
flist = list.celfiles("data/", full.names=TRUE, pattern="GSM29")
col0wd = ReadAffy(filenames = flist)

filetab = read.csv("data/dataVanDijk.csv")
rownames(filetab) = filetab$file
# Q: how many biological replicates?
table(filetab$type)

# remove the .CEL string from the sample name to then match filetab
samplenames = sampleNames(col0wd)
sampleNames(col0wd) = sub("\\.CEL$", "", samplenames)
samplenames = sampleNames(col0wd)

# add phenoData
mti = match(rownames(filetab), sampleNames(col0wd))
metad <- data.frame(labelDescription = c("Experimental condition: Watered or Drought", "Sample ID", "Replicaete ID"))
phenoData(col0wd) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)

# inspect the phenoData
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type

# Simple Quality Control
library(simpleaffy)
QC.col0wd = qc(col0wd)
plot(QC.col0wd)
# You can access each metrics through its label
slotNames(QC.col0wd)


esetRMA.col0wd = rma(col0wd)
sampleNames(esetRMA.col0wd)
head(featureNames(esetRMA.col0wd))
length(featureNames(esetRMA.col0wd))

# Make boxplots of raw and pre-processed data
boxplot(col0wd)
#boxplot(esetRMA.col0wd)

library(ath1121501.db)
ath1121501.db
# Get the environment list
ath1121501()

papergenes = c("AT4G34000", "AT5G51990", "AT4G27410", "AT5G52310", "AT5G52300", "AT2G46680", "AT3G61890")
# Obtain the probe id corresponding to papergenes[[1]]
accnum2probe = as.data.frame(ath1121501ACCNUM)
findprobe = function(gid, df){
  return(df$probe_id[df$gene_id==gid])
}
paperprobes = sapply(papergenes, findprobe, accnum2probe)

# qPCR genes
qpcrgenes = c("AT1G52690", "AT3G06420", "AT5G36910", "AT2G14750", "AT4G04610", "AT1G74090")
qpcrprobes = sapply(qpcrgenes, findprobe, accnum2probe)


# Obtain the mean expr values for each sample group
# make sure you know your sample groups
phenoData(col0wd)$type
pd = phenoData(col0wd)
# get expr values
# Now, for each of these probes we want to obtain a vector of expression value means of the sample groups (in our case, one value for Watered, one for Drought, for each probe)

expval.col0wd = exprs(esetRMA.col0wd)

# Use limma
# phenoData(col0wd)$type is already a factor object
design = model.matrix(~ 0 + phenoData(col0wd)$type)
colnames(design) = c("Drought", "Watered")
dfit = lmFit(expval.col0wd, design)
# contrast matrix
cm = makeContrasts(Drought-Watered,levels=design)
dfit.con = contrasts.fit(dfit, cm)
# Compute moderated t-test on each probe; check t-statistics and p-values
dfit.eb = eBayes(dfit.con)
dfit.eb$coefficients[1:10,]
dfit.eb$t[1:10,]
dfit.eb$p.value[1:10,]

name = "Volcano.jpg"
jpeg(name)
volcanoplot(dfit.eb,coef=1,highlight=20)
dev.off()

# correct for multiple testing
genetab.col0wd = topTable(dfit.eb,coef=1,number=length(dfit.eb),adjust.method="BH")
siggenetab.col0wd = genetab.col0wd[genetab.col0wd$adj.P.Val < 0.001, ]
upregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC > 1,]
downregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC < -1,]
dim(downregtab.col0wd)
dim(upregtab.col0wd)

# Plot a heatmap of the selected genes
sev = expval.col0wd[rownames(siggenetab.col0wd), ]
heatmap(sev)











