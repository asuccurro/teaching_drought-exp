source('~/repositories/gitlab/teaching_drought-exp/r_scripts/load_and_qc.r')
# Create a file for Ron1 exp
f = read.csv("data/E-TABM-566.sdrf.txt", sep = '\t')
colnames(f)
f$Factor.Value..genotype.
f$Sample.Name
f$Array.Data.File
fl = paste("data/", f$Array.Data.File, sep="")
tf = f[, c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
colnames(tf) = c("type","file","replicate")
tf$file = sub("\\.CEL", "", tf$file)
#rownames(tf) = tf$file
tf$type = sub("wild type", "wt", tf$type)
tf$type = sub("ron1-1", "ron1", tf$type)

write.csv(tf, "data/dataRobles.csv", quote=FALSE, row.names = FALSE)

lerron1 = loadAndFormatAffyData("data/", "hyb3", "data/dataRobles.csv", "Wild Type or ron1 mutant")
esetRMA.lerron1 = doQCAndRMAOnAffyData(lerron1, "outputs/")
