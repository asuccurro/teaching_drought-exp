
#library(oligo)
#raw_data <- read.celfiles(filenames = list.celfiles("data/", full.names=TRUE, pattern="GSM290"))
#esetOligoRMA.col0wd = oligo::rma(raw_data)
#expval.col0wd = exprs(esetOligoRMA.col0wd)
#expval.col0wd[1:10,]

# Set cutoff
?rowMedians
emedians = rowMedians(esetRMA.col0wd)

emedians_hist = hist(emedians, 100, col="#e7efd8", freq = FALSE,
                     main = "Histogram of the median intensities",
                     xlab = "Median intensities")

#ix = which.max(emedians_hist$density[20:length(emedians_hist$density)])
#emp_mu = emedians_hist$breaks[20+ix]
#emp_sd = mad(emedians)/2

rescale = 0.4
emp_mu = 6.2
emp_sd = mad(emedians)/2

lines(sort(emedians), rescale*dnorm(sort(emedians),mean = emp_mu , sd = emp_sd),col = "grey10", lwd = 4)

cutoff = 0.05 / rescale
thresh_median = qnorm(cutoff, emp_mu, emp_sd)
samples_cutoff = 3

idx_thresh_median <- apply(esetRMA.col0wd, 1, function(x){sum(x > thresh_median) >= samples_cutoff})
table(idx_thresh_median)


filtered_esetRMA.col0wd <- subset(esetRMA.col0wd, idx_thresh_median)
emedians = rowMedians(filtered_esetRMA.col0wd)
emedians_hist = hist(emedians, 100, col="#e7efd8", freq = FALSE,
                     main = "Histogram of the median intensities",
                     xlab = "Median intensities")

