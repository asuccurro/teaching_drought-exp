setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")
library(ath1121501.db)
library(affyPLM)
library(RColorBrewer)

findprobe = function(gid, df){
  return(df$probe_id[df$gene_id==gid])
}
