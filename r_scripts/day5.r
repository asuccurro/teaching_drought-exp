# Day 5 - functional enrichment

source('~/repositories/gitlab/teaching_drought-exp/r_scripts/day4.r')

source("https://bioconductor.org/biocLite.R")
biocLite("clusterProfiler")

#biocLite("DO.db")
#biocLite("DOSE")


remove.packages("DOSE", lib=.libPaths())
library(clusterProfiler)
head(fullDE.c24fry1)

kt = keytypes(ath1121501.db)

ids = bitr(rownames(fullDE.col0wd), fromType = "PROBEID", toType = c("TAIR"), OrgDb = "ath1121501.db")
ind = which(duplicated(ids$PROBEID) == FALSE)
ids = ids[ind,]

run_enrich_GO = function(resultsDE, idmap, fdrThr){
  ade = merge(x = resultsDE, y = idmap, by.x="row.names", by.y="PROBEID")
  sde = subset(ade, adj.P.Val < fdrThr)
  head(ade)
  # list of significant gene ids
  sgl = as.character(sde$TAIR)
  # list of all matched gene ids
  agl = as.character(ade$TAIR)
  
  # subontology = Biological Process
  # newer clusterProfiles
  ego = enrichGO(gene = sgl, universe = agl, keyType = "TAIR", OrgDb = ath1121501.db, ont="BP", pAdjustMethod = "BH", qvalueCutoff = fdrThr, readable = TRUE)
  # older clusterProfiles
  #ego = enrichGO(gene = sgl, universe = agl, keytype = "TAIR", OrgDb = ath1121501.db, ont="BP", readable = TRUE)
  return(ego)
}


ade = merge(x = fullDE.col0alx8, y = ids, by.x="row.names", by.y="PROBEID")
sde = subset(ade, adj.P.Val < fdrThr)
head(ade)


myFDR=0.05

mygo_wd = run_enrich_GO(fullDE.col0wd, ids, myFDR)
mygo_ron1 = run_enrich_GO(fullDE.col0ron1, ids, myFDR)
mygo_alx8 = run_enrich_GO(fullDE.col0alx8, ids, myFDR)
mygo_fry1 = run_enrich_GO(fullDE.c24fry1, ids, myFDR)




png("outputs/go_wd.png")
dotplot(mygo_wd, showCategory=30, font.size = 8)
dev.off()

png("outputs/go_ron1.png")
dotplot(mygo_ron1, showCategory=30, font.size = 8)
dev.off()

png("outputs/go_alx8.png")
dotplot(mygo_alx8, showCategory=30, font.size = 8)
dev.off()

png("outputs/go_fry1.png")
dotplot(mygo_fry1, showCategory=30, font.size = 8)
dev.off()

############################################################3
#px is the list of DE (PROBEID) in all 3 samples
sharedDE = fullDE.c24fry1[px,]
mygo = run_enrich_GO(sharedDE, ids, myFDR)
dotplot(mygo, showCategory=10, font.size = 8)


x = fullDE.col0alx8
xyz = mutate(x, APV=ifelse(x$`adj.P.Val`<myFDR, x$`adj.P.Val`, 999.))
rownames(xyz) = rownames(x)

y = xyz[xyz$`adj.P.Val` > myFDR,]
head(xyz)
head(x)
head(y)

ade = merge(x = xyz, y = ids, by.x="row.names", by.y="PROBEID")
head(ade)
sde = subset(ade, APV < myFDR)
head(ade)
head(sde)
dim(sde)
#sde = sde[sde$Row.names == px]
dim(sde)
# list of significant gene ids
sgl = as.character(sde$TAIR)
# list of all matched gene ids
agl = as.character(ade$TAIR)

# subontology = Biological Process
ego = enrichGO(gene = sgl, universe = agl, keytype = "TAIR", OrgDb = ath1121501.db, ont="BP", pAdjustMethod = "BH", qvalueCutoff = myFDR, readable = TRUE)
dotplot(ego, showCategory=50, font.size = 8)



