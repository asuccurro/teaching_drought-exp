# DAY 1 - just set up and read docs

source("http://www.bioconductor.org/biocLite.R")
biocLite(c('affy', 'affydata', 'affyPLM', 'simpleaffy', 'limma', 'ath1121501.db', 'ath1121501cdf', 'ath1121501probe'))


library(limma)
library(affy)

setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")
flist = list.celfiles("data/", full.names=TRUE, pattern="GSM29")
col0wd = ReadAffy(filenames = flist)
vignette('affy')
