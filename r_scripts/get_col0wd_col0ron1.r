
source('~/repositories/gitlab/teaching_drought-exp/r_scripts/load_and_qc.r')


col0wd = loadAndFormatAffyData("data/", "GSM290", "data/dataVanDijk.csv", "Watered or Drought")
# inspect the phenoData
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type

esetRMA.col0wd = doQCAndRMAOnAffyData(col0wd, "outputs/")




# Create a file for Ron1 exp
f = read.csv("data/E-TABM-566.sdrf.txt", sep = '\t')
colnames(f)
f$Factor.Value..genotype.
f$Sample.Name
f$Array.Data.File
fl = paste("data/", f$Array.Data.File, sep="")
tf = f[, c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
colnames(tf) = c("type","file","replicate")
tf$file = sub("\\.CEL", "", tf$file)
#rownames(tf) = tf$file
tf$type = sub("wild type", "wt", tf$type)
tf$type = sub("ron1-1", "ron1", tf$type)

write.csv(tf, "data/dataRobles.csv", quote=FALSE, row.names = FALSE)

col0ron1 = loadAndFormatAffyData("data/", "hyb3", "data/dataRobles.csv", "Wild Type or ron1 mutant")
esetRMA.col0ron1 = doQCAndRMAOnAffyData(col0ron1, "outputs/")







#fry1 wrt C24
#alx8 wrt col0

f = read.csv("data/E-MEXP-1495.sdrf.txt", sep="\t")
colnames(f)
f$Factor.Value..genotype.
f$Sample.Name
f$Array.Data.File
fl = paste("data/", f$Array.Data.File, sep="")
tf = f[, c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
tf
colnames(tf) = c("type","file","replicate")
tf$file = sub("\\.CEL", "", tf$file)
#rownames(tf) = tf$file
tf$type = sub(" 3", "", tf$replicate)
tf$type = sub(" 2", "", tf$type)
tf$type = sub(" 1", "", tf$type)
tf$type = sub("fry1-1", "fry1", tf$type)
tf$type = sub("Col-0", "wt", tf$type)

write.csv(tf, "data/dataWilson.csv", quote=FALSE, row.names = FALSE)

col0alx8 = loadAndFormatAffyData("data/", fl, "data/dataWilson.csv", "Wild Type Col0, alx8 mutant, Wild Type C24, fry1 mutant", TRUE)
esetRMA.col0alx8 = doQCAndRMAOnAffyData(col0alx8, "outputs/")
