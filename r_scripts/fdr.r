# FDRs

# Set the standard FDR threshold 5%
FDR = 0.05

# These are the p values from the gene-wise t-tests (eBayes)
pv = eb.col0wd$p.value


totgenes = length(pv)
pv.siggenes = length(pv[pv < FDR])
pv.falsepos = FDR*length(pv)
pv.ratio = pv.falsepos / pv.siggenes

# These are the adjusted p values from the BH correction (topTable)
apv = fullDE.col0wd$adj.P.Val
apv.siggenes = length(apv[apv < FDR])
apv.falsepos = FDR*length(apv[apv < FDR])
apv.ratio = apv.falsepos / apv.siggenes

df = data.frame(matrix(0, 4, 2))
colnames(df) = c("p value", "adj. p value")
rownames(df) = c("tot. genes", "n. sign. genes", "n. false pos.", "% of false pos.")
df$`p value` = as.integer( c(totgenes, pv.siggenes, pv.falsepos, 100*(pv.ratio)))
df$`adj. p value` = as.integer( c(totgenes, apv.siggenes, apv.falsepos, 100*(apv.ratio)))

df
