#https://f1000research.com/articles/5-1384/v1

head(pData(col0wd))
head(exprs(col0wd))

makePCAplot = function(datamat, label, title){
  pca_all = prcomp(datamat, scale = FALSE)
  dataGG = data.frame(PC1 = pca_all$x[,1], PC2 = pca_all$x[,2], Sample = label)
  ( qplot(PC1, PC2, data = dataGG, color = Sample, main = title)
  + scale_colour_brewer(palette = "Set2")
  )
  #x = list(pca_all, dataGG)
  #return(x)
}

png("outputs/pca_raw.png")
makePCAplot(t(exprs(col0wd)), pData(col0wd)$type, "PCA on raw data")
dev.off()

png("outputs/pca_norm.png")
makePCAplot(t(exprs(esetRMA.col0wd)), pData(esetRMA.col0wd)$type, "PCA on norm data")
dev.off()

