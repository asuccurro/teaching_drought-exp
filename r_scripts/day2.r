# Day 2 - check data structures

setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")

library(affydata)
data("Dilution")
Dilution

library(affy)
flist = list.celfiles("data/", full.names=TRUE, pattern="GSM29")
col0wd = ReadAffy(filenames = flist)

cdfName(Dilution)
cdfName(col0wd)

dim(Dilution)
dim(col0wd)

experimentData(Dilution)
experimentData(col0wd)

featureData(Dilution)
featureData(col0wd)

phenoData(Dilution)
phenoData(col0wd)

pData(Dilution)
pData(col0wd)

source('~/repositories/gitlab/teaching_drought-exp/r_scripts/load_and_qc.r')
col0wd = loadAndFormatAffyData("data/", "GSM290", "data/dataVanDijk.csv", "Watered or Drought")

# inspect the phenoData
phenoData(col0wd)
pData(col0wd)
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type

esetRMA.col0wd = doQCAndRMAOnAffyData(col0wd, "outputs/")


