get_differential_expressions = function(expval, dmatrix){
  if (!all(c("Control", "Test") %in% colnames(dmatrix))){
    message("WARNING: Levels should be named Control and Test. Exiting now.")
    return()
  }
  library(limma)
  # Define contrast matrix
  cm = makeContrasts(Test-Control,levels=dmatrix)
  # First fit
  dfit = lmFit(expval, dmatrix)
  # Fit contrast
  dfit = contrasts.fit(dfit, cm)
  return(eBayes(dfit))
}
