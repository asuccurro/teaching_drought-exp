# Day 3 - expression values analysis

############# 3.a Recap from day 2
setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")
source('~/repositories/gitlab/teaching_drought-exp/r_scripts/load_and_qc.r')
col0wd = loadAndFormatAffyData("data/", "GSM290", "data/dataVanDijk.csv", "Watered or Drought")

phenoData(col0wd)
pData(col0wd)
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type


esetRMA.col0wd = affy::rma(col0wd)

sampleNames(esetRMA.col0wd)
head(featureNames(esetRMA.col0wd))
length(featureNames(esetRMA.col0wd))
phenoData(esetRMA.col0wd)
pData(esetRMA.col0wd)

# Probes to Genes
library(ath1121501.db)

ath1121501.db
# Get the environment list
ath1121501()
# Select a probe id and have a look at some environment
x  = "245035_at"
ath1121501GENENAME[[x]]
ath1121501ENZYME[[x]]
ath1121501SYMBOL[[x]]
ath1121501ACCNUM[[x]]


# Genes of interest in van Dijk et al.
papergenes = c("AT4G34000", "AT5G51990", "AT4G27410", "AT5G52310", "AT5G52300", "AT2G46680", "AT3G61890")

# Obtain the probe id corresponding to papergenes[[1]]
accnum2probe = as.data.frame(ath1121501ACCNUM)
findprobe = function(gid, df){
  return(df$probe_id[df$gene_id==gid])
}
paperprobes = sapply(papergenes, findprobe, accnum2probe)

# qPCR genes
qpcrgenes = c("AT1G52690", "AT3G50980", "AT3G06420", "AT2G37640", "AT5G36910", "AT2G14750", "AT4G04610", "AT2G20610", "AT3G45140", "AT1G74090")
qpcrprobes = sapply(qpcrgenes, findprobe, accnum2probe)


############# 3.b new figures

# Make boxplots of raw and pre-processed data
png(paste("outputs/", cdfName(col0wd), "_raw_boxplot.png", sep=""))
boxplot(col0wd)
dev.off()

library(affyPLM)
png(paste("outputs/", cdfName(col0wd), "_norm_boxplot.png", sep=""))
boxplot(esetRMA.col0wd)
dev.off()

# Make heatmap from manhattan distance
library(RColorBrewer)

dists = as.matrix(dist(t(exprs(esetRMA.col0wd)), method = "manhattan"))

colnames(dists) = paste(pData(esetRMA.col0wd)$type, c(1,2,3,1,2,3))
rownames(dists) = paste(pData(esetRMA.col0wd)$type, c(1,2,3,1,2,3))

hmcol = colorRampPalette(rev(brewer.pal(9, "Blues")))(255)
#hmcol = colorRampPalette(rev(brewer.pal(9, "PuOr")))(255)

png("outputs/dist_heatmap.png")
heatmap(dists, col = rev(hmcol), distfun = function(x) dist(x,method="manhattan"))
dev.off()

heatmap(dists, col = rev(hmcol), distfun = function(x) dist(x,method="euclidean"))
heatmap(exprs(esetRMA.col0wd), distfun = function(x) dist(x,method="manhattan"))


############# 3.c expression values

library(limma)

# get expr values
expval.col0wd = exprs(esetRMA.col0wd)

#################### i) design matrix
# define the design matrix
# phenoData(col0wd)$type is already a factor object
#design = model.matrix(~ factor(c("W", "W", "W", "D", "D", "D")))
#design = model.matrix(~ phenoData(col0wd)$type)


### OPTION 1
design = model.matrix(~ 0 + phenoData(col0wd)$type)
design
colnames(design) = c("D", "W")
design


#### OPTION 2
design = model.matrix(~ factor(c("W", "W", "W", "D", "D", "D"), levels = c("W", "D")))
design
colnames(design) = c("(Intercept)", "D")
design
dfit = lmFit(expval.col0wd, design)


#################### ii) fit a linear model
design = model.matrix(~ 0 + phenoData(col0wd)$type)
colnames(design) = c("D", "W")
# Define contrast matrix
cm = makeContrasts(D-W,levels=design)
# First fit
dfit = lmFit(expval.col0wd, design)
# Fit contrast
dfit = contrasts.fit(dfit, cm)

# Compute moderated t-test on each probe; check t-statistics and p-values
dfit.eb = eBayes(dfit)
dfit.eb$coefficients[1:10,]
dfit.eb$t[1:10,]
dfit.eb$p.value[1:10,]

jpeg("outputs/volcano.jpg")
volcanoplot(dfit.eb,coef=1,highlight=20)
dev.off()


# correct for multiple testing
genetab.col0wd = topTable(dfit.eb,coef=1,number=dim(dfit.eb)[1],adjust.method="BH")
genetab.col0wd = topTable(dfit.eb,coef=1,number=10,adjust.method="BH")
siggenetab.col0wd = genetab.col0wd[genetab.col0wd$adj.P.Val < 0.005, ]
upregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC > 1,]
downregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC < -1,]
dim(downregtab.col0wd)
dim(upregtab.col0wd)

# Plot a heatmap of the selected genes
sev = expval.col0wd[rownames(siggenetab.col0wd), ]
hmcol = colorRampPalette(rev(brewer.pal(9, "PuOr")))(255)
jpeg("outputs/heatmap_all.jpg")
heatmap(sev, col = rev(hmcol))
dev.off()











# contrast matrix
cm = makeContrasts(W-D,levels=design)
dfit.con = contrasts.fit(dfit, cm)

# Compute moderated t-test on each probe; check t-statistics and p-values
dfit.eb = eBayes(dfit.con)
dfit.eb$coefficients[1:10,]
dfit.eb$t[1:10,]
dfit.eb$p.value[1:10,]

name = "Volcano.jpg"
jpeg(name)
volcanoplot(dfit.eb,coef=1,highlight=20)
dev.off()


library("ggplot2") #Best plots
library("ggrepel") #Avoid overlapping labels
library("dplyr")

df = as.data.frame(dfit.eb)
results = mutate(df, sig=ifelse(df$p.value<0.005, "p value<0.05", "Not Sig"))
g=rownames(dfit.eb$coefficients)
gn=sapply(g, findprobe, accnum2probe)
results = mutate(results, gene=gn)
p = ggplot(results, aes(coefficients, -log10(p.value))) +
  geom_point(aes(col=sig)) +
  scale_color_manual(values=c("red", "black"))
p
p+geom_text(data=filter(results, p.value<0.005), aes(label=gene))


# correct for multiple testing
genetab.col0wd = topTable(dfit.eb,coef=1,number=length(dfit.eb),adjust.method="BH")
siggenetab.col0wd = genetab.col0wd[genetab.col0wd$adj.P.Val < 0.001, ]
upregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC > 1,]
downregtab.col0wd = siggenetab.col0wd[siggenetab.col0wd$logFC < -1,]
dim(downregtab.col0wd)
dim(upregtab.col0wd)

# Plot a heatmap of the selected genes
sev = expval.col0wd[rownames(siggenetab.col0wd), ]
heatmap(sev)

# Get list of probes differentialy expressed deprobs.list
#deprobs.symbol = unlist( mget(deprobs.list, envir = ath1121501SYMBOL) )

