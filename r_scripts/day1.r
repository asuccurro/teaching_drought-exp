setwd("/home/succurro/repositories/gitlab/teaching_drought-exp/")

library(affy)
flist = list.celfiles("data/", full.names=TRUE, pattern="GSM29")
col0wd = ReadAffy(filenames = flist)

col0wd
class(col0wd)
?AffyBatch

library(affydata)
data("Dilution")
Dilution

cdfName(Dilution)
cdfName(col0wd)

dim(Dilution)
dim(col0wd)

experimentData(Dilution)
experimentData(col0wd)

featureData(Dilution)
featureData(col0wd)

phenoData(Dilution)
phenoData(col0wd)

pData(Dilution)
pData(col0wd)


# get a dataframe connecting file names with experiments; make the rows named as the file
filetab = read.csv("data/dataVanDijk.csv")
rownames(filetab) = filetab$file
# Q: how many biological replicates?
table(filetab$type)

# remove the .CEL string from the sample name to then match filetab
samplenames = sampleNames(col0wd)
sampleNames(col0wd) = sub("\\.CEL$", "", samplenames)
samplenames = sampleNames(col0wd)


# add phenoData
mti = match(rownames(filetab), sampleNames(col0wd))
metad <- data.frame(labelDescription = c("Experimental condition: Watered or Drought", "Sample ID", "Replicate ID"))
phenoData(col0wd) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)

# inspect the phenoData
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type



boxplot(Dilution)
image(Dilution)

library(simpleaffy)
#qcdil = qc(Dilution)
#plot(qcdil)
#slotNames(qcdil)
#qcdil@percent.present





library(simpleaffy)
image(col0wd)

QC.col0wd = qc(col0wd)

plot(QC.col0wd)

slotNames(QC.col0wd)
QC.col0wd@percent.present



esetRMA.col0wd = rma(col0wd)
boxplot(col0wd)
boxplot(esetRMA.col0wd)


for (i in 1:2)
{
  name = paste("outputs/drought_MAplot_",samplenames[i],".jpg",sep="")
  jpeg(name)
  MAplot(col0wd,which=i)
  dev.off()
  name = paste("outputs/drought_MAplot_",samplenames[i],"_norm.jpg",sep="")
  jpeg(name)
  MAplot(esetRMA.col0wd,which=i)
  dev.off()
}





