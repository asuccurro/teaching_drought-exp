source("http://www.bioconductor.org/biocLite.R")
biocLite(c('affy', 'affydata', 'affyPLM', 'simpleaffy', 'limma', 'ath1121501.db', 'ath1121501cdf', 'ath1121501probe', 'oligo'))
install.packages(c("ggplot2", "ggrepel", "dplyr"))
