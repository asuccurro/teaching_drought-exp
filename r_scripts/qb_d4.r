# DAY 4

# alx8

f = read.csv("data/E-MEXP-1495.sdrf.txt", sep = '\t')
f$Factor.Value..ecotype.
f$Array.Data.File
fl = paste("data/", f$Array.Data.File[f$Factor.Value..ecotype. == "Columbia-0"], sep="")
col0alx8 = ReadAffy(filenames = fl)

tf = f[f$Factor.Value..ecotype. == "Columbia-0", c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
colnames(tf) = colnames(filetab)
tf$file = sub("\\.CEL", "", tf$file)
rownames(tf) = tf$file
tf$type = sub("wild type", "wt", tf$type)

d2 = function(affyobj, filetab, expcond){
  samplenames = sampleNames(affyobj)
  sampleNames(affyobj) = sub("\\.CEL$", "", samplenames)
  samplenames = sampleNames(affyobj)
  # add phenoData
  mti = match(rownames(filetab), sampleNames(affyobj))
  metad <- data.frame(labelDescription = c(paste("Experimental condition: ", expcond), "Sample ID", "Replicate ID"))
  phenoData(affyobj) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)
  # inspect the phenoData
  print(varMetadata(phenoData(affyobj)))
  print(phenoData(affyobj)$type)
  return(affyobj)
}

col0alx8 = d2(col0alx8, tf, "Wild Type or alx8 mutant")

## QC 
QC.col0alx8 = qc(col0alx8)
plot(QC.col0alx8)

## Preproc

esetRMA.col0alx8 = rma(col0alx8)
sampleNames(esetRMA.col0alx8)
head(featureNames(esetRMA.col0alx8))
length(featureNames(esetRMA.col0alx8))

# Make boxplots of raw and pre-processed data
boxplot(col0alx8)
#boxplot(esetRMA.col0alx8)

## Compute expr
expval.col0alx8 = exprs(esetRMA.col0alx8)
design = model.matrix(~ 0 + phenoData(col0alx8)$type)
colnames(design) = c("Mutant", "WildType")
dfit2 = lmFit(expval.col0alx8, design)
# contrast matrix
cm = makeContrasts(Mutant-WildType,levels=design)
dfit2.con = contrasts.fit(dfit2, cm)
# Compute moderated t-test on each probe; check t-statistics and p-values
dfit2.eb = eBayes(dfit2.con)
dfit2.eb$coefficients[1:10,]
dfit2.eb$t[1:10,]
dfit2.eb$p.value[1:10,]
name = "VolcanoAlx8WT.jpg"
jpeg(name)
volcanoplot(dfit2.eb,coef=1,highlight=20)
dev.off()

# correct for multiple testing
genetab.col0alx8 = topTable(dfit2.eb,coef=1,number=length(dfit2.eb),adjust.method="BH")
siggenetab.col0alx8 = genetab.col0alx8[genetab.col0alx8$adj.P.Val < 0.001, ]
upregtab.col0alx8 = siggenetab.col0alx8[siggenetab.col0alx8$logFC > 1,]
downregtab.col0alx8 = siggenetab.col0alx8[siggenetab.col0alx8$logFC < -1,]
dim(downregtab.col0alx8)
dim(upregtab.col0alx8)
# Plot a heatmap of the selected genes
sev = expval.col0alx8[rownames(siggenetab.col0alx8), ]
heatmap(sev)

gtALX8 = as.data.frame(genetab.col0alx8$adj.P.Val)
gtALX8 = mutate(gtALX8, col0alx8=ifelse(gtALX8$`genetab.col0alx8$adj.P.Val`<0.001, TRUE, FALSE))
rownames(gtALX8) =rownames(genetab.col0alx8)

gtwd = as.data.frame(genetab.col0wd$adj.P.Val)
gtwd = mutate(gtwd, col0wd=ifelse(gtwd$`genetab.col0wd$adj.P.Val`<0.001, TRUE, FALSE))
rownames(gtwd) =rownames(genetab.col0wd)

sgdf = data.frame(col0wd=gtwd[accnum2probe$probe_id, "col0wd"], col0alx8=gtALX8[accnum2probe$probe_id, "col0alx8"])
rownames(sgdf) = accnum2probe$probe_id


