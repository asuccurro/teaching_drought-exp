# DAY 2 - start working on the data

# get a dataframe connecting file names with experiments; make the rows named as the file
filetab = read.csv("data/dataVanDijk.csv")
rownames(filetab) = filetab$file
# Q: how many biological replicates?
table(filetab$type)

# remove the .CEL string from the sample name to then match filetab
samplenames = sampleNames(col0wd)
sampleNames(col0wd) = sub("\\.CEL$", "", samplenames)
samplenames = sampleNames(col0wd)

# add phenoData
mti = match(rownames(filetab), sampleNames(col0wd))
metad <- data.frame(labelDescription = c("Experimental condition: Watered or Drought", "Sample ID", "Replicaete ID"))
phenoData(col0wd) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)

# inspect the phenoData
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type

# Simple Quality Control
image(col0wd)
library(simpleaffy)
QC.col0wd = qc(col0wd)
plot(QC.col0wd)
# You can access each metrics through its label
slotNames(QC.col0wd)
QC.col0wd@percent.present

# Extra
library("affyPLM")
dataPLM <- fitPLM(col0wd)
boxplot(dataPLM, main = "NUSE", ylim = c(0.95, 1.22), outline = FALSE, col = "lightblue", las = 3, whisklty = 0, staplelty = 0)
Mbox(dataPLM, main = "RLE", ylim = c(-0.4, 0.4), outline = FALSE, col = "mistyrose", las = 3, whisklty = 0, staplelty = 0)


esetRMA.col0wd = rma(col0wd)

for (i in 1:6)
{
  name = paste("outputs/drought_MAplot",i,".jpg",sep="")
  jpeg(name)
  MAplot(col0wd,which=i)
  dev.off()
  name = paste("outputs/drought_MAplotnorm",i,".jpg",sep="")
  jpeg(name)
  MAplot(esetRMA.col0wd,which=i)
  dev.off()
}

