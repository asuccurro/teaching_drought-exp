# Analyze the data from Wilson et al. 2009

Download the raw data and the Sample and data relationship files from
[E-MEXP-1495](https://www.ebi.ac.uk/arrayexpress/experiments/E-MEXP-1495/).
We want to analyze only the Columbia0 ecotypes, wild type and drought resistant alx8 mutant.

```R
f = read.csv("data/E-MEXP-1495.sdrf.txt", sep = '\t')
f$Factor.Value..ecotype.
f$Array.Data.File
fl = paste("data/", f$Array.Data.File[f$Factor.Value..ecotype. == "Columbia-0"], sep="")
col0alx8 = ReadAffy(filenames = fl)
```

Here a little help on setting up the table file:
```R
tf = f[f$Factor.Value..ecotype. == "Columbia-0", c("Factor.Value..genotype.", "Array.Data.File", "Sample.Name")]
colnames(tf) = colnames(filetab)
tf$file = sub("\\.CEL", "", tf$file)
rownames(tf) = tf$file
tf$type = sub("wild type", "wt", tf$type)
```

## Assignments

Now the floor is yours. Perform the same analysis we did for Col0 Watered and Drought data. You can either rewrite your previous scripts to use the new data on Col0 WT and alx8 mutant, or (preferred, slightly more advanced) restructure the lines of code we wrote into functions that can in future be called to perform differential expression analysis on any affymetrix data with two sample groups. If you go with this option, keep in mind that you will have to remove any "hardcoding" of variable names, they will either have to be passed as function arguments or obtained by reading attributes / calling methods of objects.

Have fun!

### Make a heatmap of the selected genes

For the Col0 Watered and Drought data, we can obtain a heatmap as:

```R
sev = expval.col0wd[rownames(siggenetab), ]
heatmap(sev)
```

1. Obtain a heatmap for the Col0 WT and alx8 mutant selecting the significant genes you obtained for those data (equivalent to the rownames(siggenetab)).
2. Make a Venn diagram (search for the relevant instructions!) to see if there are shared genes among the two analyses (same organism in watered/drought condition, two organisms in standard condition). What could that tell us?
3. Create a matrix with as rows the shared significant genes of the two datasets and as colums the total twelve samples. Plot it as a heatmap. What do you see?
