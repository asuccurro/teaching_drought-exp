Linear models are nothing more than linear equations fitting the data and allowing us to predict Y from value X:

	```math
	Y = kX + e
	```

	In our case, we want to predict if a gene is from "Watered" or "Drought" samples. W and D groups are then also called
	predictors, or factors. Other examples would be "Wild Type" or "Mutant", etc.

	Our linear models (done gene by gene in limma) will look like:
	```math
	gene(W) = mean(W) + err
	gene(D) = mean(D) + err
	```

In "mathematical form" it's like:
	```math
	gW = 1*mW + 0*mD + err
	gD = 0*mW + 1*mD + err
	```

Now if we plug in the intensity data for our first gene we have a system of equations like:
	```math
4.345122 = 1*mW + 0*mD + err
5.185281 = 1*mW + 0*mD + err
4.925762 = 1*mW + 0*mD + err
6.083858 = 0*mW + 1*mD + err
7.758459 = 0*mW + 1*mD + err
6.952316 = 0*mW + 1*mD + err
	```
This is where the design matrix comes from.


With this design, the model parameters we will get are the means of the two groups. 

```R
design = model.matrix(~ 0 + phenoData(col0wd)$type)
colnames(design) = c("Drought", "Watered")
dfit = lmFit(expval.col0wd, design)
expval.col0wd[1,]
dfit$coefficients[1,]
```

If we are interested in comparing the two groups (differential expression analysis), we have either to define a different design or to add a contrast afterwards. Let's say we are interested in D vs W, and specifically the difference mD - mW:
```math
	gW = 1*mW + 0*mD + err
	gD = 0*mW + 1*mD + err = 1*mW + 1*mD - 1*mW + err = 1*mW + 1*dM + err
```

And now the design matrix will look like:
	```math
4.345122 = 1*mW + 0*dM + err
5.185281 = 1*mW + 0*dM + err
4.925762 = 1*mW + 0*dM + err
6.083858 = 1*mW + 1*dM + err
7.758459 = 1*mW + 1*dM + err
6.952316 = 1*mW + 1*dM + err
	```


	
	We want to do ANOVA, which in the simplest form is a t-test, where the difference between the group means is compared
	to the variability within the groups.

	