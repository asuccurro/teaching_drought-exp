# QB Practica, Universität zu Köln July 2017

Welcome to the wiki for the data analysis part of the practica! Here you will find material guiding you through the computational part of the course. You can contact your tutor (me) Dr Antonella Succurro any time at the address (remove spaces and use the @ symbol)

```bash
 a . succurro AT uni - koeln . de
```

I will be at UniKö in the weeks 10-14 and 17-21 of July.


## References and external Material

You will be analyzing affymetrix data from the work of [van Dijk et al, 2010](https://www.ncbi.nlm.nih.gov/pubmed/21050490) obtained on Col0 under control and drought conditions. You will then compare the gene expression levels with data obtained on mutants.

You can find the datasets in:
* https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE11538
* https://www.ebi.ac.uk/arrayexpress/experiments/E-TABM-566/
* https://www.ebi.ac.uk/arrayexpress/experiments/E-MEXP-1495/

## Prerequisites

You should be familiar with basic statistical tests and know already basics of coding with the R language. For a quick review you can find your favorite tutorial online or use this introductory material:
* [starting-r.pdf](/uploads/ed810109849147147e380321b64f47b2/starting-r.pdf)
* [control-functions.pdf](/uploads/63ecf1c48bec71a0e2c01f0c997ab303/control-functions.pdf)


If you did not install R Studio before, now it's the time to do so! Find the instructions online specific for your Operating System.

## Resources

You are warmly encouraged to independently retrieve information and find solutions. The internet is overwhelmed with tutorials and resources, which also means that we cannot always confide in the quality of such material. With experience and critical judgement you can decide on your own which sources to trust, but in the meantime here a (non comprehensive) list of community-trusted website.

General websites:
* stackoverflow
* gitlab and github
* universities website with class material
* online learning platforms like edx.org and coursera.org

R specific:
* bioconductor (forum, documentation etc.)
* the R studio interactive documentation that you access through a syntax like

```R
?AffyBatch
```

## Index

* [Day 1: setup your environment and get familiar with the documentation](setupandRTD)
* [Day 2: start processing the data](dataProc)
