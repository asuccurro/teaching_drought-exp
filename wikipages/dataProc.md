# Day 2 - Start with data pre-processing

We will now proceed with the first steps of data pre-processing.

Download this file [dataVanDijk.csv](/uploads/307727e6006d5b4284739f427c9ff8a5/dataVanDijk.csv) into your /MYDIR/MYPRACTICA/data/ folder. It is a table containing the sample file names, their replicate ID and a type label that is either W (watered) or D (drought).

## Names and corresponding treatment

We start doing some basic renaming to link the .CEL files to the experimental condition.

```R
# get a dataframe connecting file names with experiments; make the rows named as the file
filetab = read.csv("data/dataVanDijk.csv")
rownames(filetab) = filetab$file
# Q: how many biological replicates?
table(filetab$type)

# remove the .CEL string from the sample name to then match filetab
samplenames = sampleNames(col0wd)
sampleNames(col0wd) = sub("\\.CEL$", "", samplenames)
samplenames = sampleNames(col0wd)


# add phenoData
mti = match(rownames(filetab), sampleNames(col0wd))
metad <- data.frame(labelDescription = c("Experimental condition: Watered or Drought", "Sample ID", "Replicaete ID"))
phenoData(col0wd) <- new("AnnotatedDataFrame", data = filetab[mti, ], varMetadata = metad)

# inspect the phenoData
varMetadata(phenoData(col0wd))
phenoData(col0wd)$type
```

## Quality Control

We first have a look (literally!) at the affymetrix using the image method

```R
image(col0wd)
```

Check [this website](http://plmimagegallery.bmbolstad.com/) for some cool images of chips with artifacts. Sometimes visualization says everything!

Now, QC will tell us if there are samples "where something went wrong" based on statistical figures defined by experts of the specific type of data. We will use the package simpleaffy to obtain a plot of these statistical metrics for affymetrix data. To interprete the plot you will have to read the method documentation:  [QCandSimpleaffy.pdf](/uploads/00dd99a61853c8ebffae498304e4817c/QCandSimpleaffy.pdf).

```R
library(simpleaffy)
QC.col0wd = qc(col0wd)
plot(QC.col0wd)
```

You should see a plot where no metric is flagged as outlier, therefore we can continue to use all of the samples. As an example, we can look more into one of these metrics, the one that returns the percentage value next to the sample name.

```R
slotNames(QC.col0wd)
QC.col0wd@percent.present
```

A probe pair is "present" if PM is signicantly larger than MM.
At least between 35% and 55% of probe pairs should be "present".

### Want more?

The affyPLM (Probe Level Modeling) package performs two other QC methods: Relative Log Expression (RLE) and
Normalize Unscaled Standard Error (NUSE). The results are then shown as boxplots to point out outlier samples.
Look up its documentation (see the method fitPLM) and check the col0wd dataset.

## Robust Multi-array Analysis RMA

RMA is implemented in the affy package and provides three steps of the data pre-processing work-flow:
1. Background correction, to remove uninteresting signals (signal vs noise)
2. Normalization across arrays, to adjust for technical variability
3. Expression calculation, to get gene expression values across the array

Each step can be switched off by setting to FALSE the respective flag (see the rma method help for more details). You can read a bit more about RMA for example [here](http://www.plexdb.org/modules/documentation/RMAexplained.pdf)

```R
esetRMA.col0wd = rma(col0wd)
```

Questions:
1. What type of object is returned by the rma method?
2. Explore the functionalities of that class
3. Obtain box plots of raw and pre-processed data (use the boxplot method)

### Want more?

#### Other methods
RMA is not the only option to perform these pre-processing, even though it is often considered the best option. The other options are the original MAS method developed by Affymetrix and the GSRMA, which is a refinement to the RMA method. All the methods are implemented in the affy package. If you are interested, search what are the differences among those methods in terms of background correction, normalization and calculation of expression values.


#### Bland–Altman plot (MA plot)

MA plots show M values (log ratios) vs A values (mean averages) of pair-wise differences of measurements in samples. We can use MA plot to show in each chip how much the variability in expression depends on the expression level defining:

```math
M = \log_{2} (PM_{i_{array}}) - \log_{2} (PM_{i_{median array}})
A = (\log_{2} (PM_{i_{array}}) + \log_{2} (PM_{i_{median array}}))/2
```

You can use the MAplot plot to observe the differences between raw and pre-processed data.
