# Day 5 - Biological interpretation of results

Computational analyses like the one we performed during the practica will in general return sets of interesting (statistically significant) genes. It is important for interpretation of the results to understand the function of these genes, and for this annotation information is taken from [Gene Ontology (GO)](http://www.geneontology.org/) and
pathway information from [KEGG](http://www.genome.jp/kegg/). Browse these website if you don't know them yet and see what kind of information and tools are available.


## Assignment: functional annotation and enrichment analysis

One interesting question is to understand if there is a statistically significant *enrichment* in up/down-regulated genes for particular pathways. For this there are several tools (also web tools) available but the steps to follow are rather simple:
1. Get a list of differentially expressed (DE) genes
2. Annotate them, e.g. with GO or KEGG, and create a contingency table
3. Do a [Fisher exact test]() for the pathways/function representation

Step 1 we did already, so we must now perform 2 and 3.

### Contingency table

You need to create a table of the format

<|     | N. DE genes     | N. not DE genes | N. Total |
| :------------- | :------------- | :------------- | :------------- |
| Pathway A       |    X_A | Y_A | X_A+Y_A    |
| Pathway B       |    X_B | Y_B | X_B+Y_B    |
| ...       |  ... | ... | ...  |>

In order to do so you have to consider the genes 

## Answer the following questions

1. Consider your results on the Col0 data: are they consistent with the ones reported by Van Dijk et al.?
2. Consider your results on the alx8 mutant: how do they compare to Col0?
3. After your lab part of the practica, come back to your computational results: what is the overall picture?

















## Congratulations for completing the computational part of the practica!
