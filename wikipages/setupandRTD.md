# Day 1 - Setup and read the relevant documentation

## Few introductory words

During your lab training you have been already made aware of the concept of "good scientific practice". This is a concept valid for experiments run in the lab (in vivo/in vitro) as well as for those run in a CPU (in silico). When you run an experiment in the lab you will for example:
* dutily document and follow a protocol that can be replicated (in your lab as well as in others)
* plan a number of biological and technical replicates sufficient to get significant result
* collect all the results, no matter if they match your starting expectations or not

In a similar way, lets see how to deal with computational analyses.

### Document and follow  a protocol that can be replicated (in your lab as well as in others)

In computational term, a protocol is a script. You should make sure that each step in your analysis is documented well enough so that a new user (or yourself one year from now!) will understand what the script is doing. Ways to achieve this are for example:
* Give meaningful names to your variables and functions
* Use comments to explain in human language what the machine language is doing
* Create a documentation to describe how to use your scripts and functions
* (More advanced) use a revision control system like git to keep track of your project evolution (equivalent to a lab book)
* (More advanced) ensure that your scripts are platform-independent (they can run on linux/windows/ios)
* (More advanced) use and provide instructions on a virtual environment to guide the next user through software installation

### Make sure your data will give significant results

When you deal with raw data ("out of the box" data) you will have to perform quality checks and data cleaning to make sure that your data meet standard requirements and that they can be analyzed with your method of choice. As an example, noisy or corrupted data can be removed, as long as their removal is justified and documented and is consistently applied. Normalization of data is also a critical step that will ensure data obtained at different times and/or on different machines can be compared.

### Unbiased statistical analysis

Sadly, examples of tweaked experiments and data analyses are not uncommon. At the same time however it is rather easy to spot biases and manomissions of data if scripts and protocols are provided. This is a reason why open access and open source research is currently encouraged and will hopefully soon become mandatory.

Establishing a new data analysis procedure is not different from establishing a new experimental protocol, and might require many iterations and tuning before being optimal. To go from raw data to statistical observables we define a series of steps that might include threshold cuts (e.g. to remove noise) and other types of manipulations. If these steps are defined while looking at the real data it is inevitable, no matter how good our original intentions, that we might tune thresholds and parameters in order to obtain results confirmig our prior hypothesis. This is why in "blind analyses" (a standard concept particle physics) all the data pre-processing and statistical analysis is optimized on control samples (usually Monte Carlo simulated data) and only afterwards, once all the steps have been fixed, run on the real data.

With regards to the affymetrix data we will analyze in the practica we do not need to worry about this, since we will follow already widely validated data analysis protocols. It is however good to keep in mind that "fine tuning" while looking at data is to avoid.

## Affimetrix data in a nutshell

![CTQ904](/uploads/d542bea069e079d6bd8f1cc5f7021043/CTQ904.jpg)

* 1 to 20 oligonucleotides of 25 base-pairs represent a gene
* A *probe* as an oligonucleotide of 25 base-pairs
* A *Perfect Match (PM)* is when the probe is complementary to a
reference sequence of interest
* A *Mismatch (MM)* is the same as PM, but with a single base change
for the middle (13th) base
* A *Probe-pair* is a (PM,MM) pair
* A *Probe-pair set*  (identified by an *Affy ID*) is a collection of probe-pairs (1 to 20) related
to a common gene or fraction of a gene

If you want to know more, check for example these [slides by Dr Janick Mathys](http://data.bits.vib.be/pub/trainingen/MicroarraysIntro/Theory.pdf)

### Setup your working directory

If you haven't installed bioconductor yet, in R Studio console do:

```R
source("http://www.bioconductor.org/biocLite.R")
biocLite()
```

This will install basic bioconductor packages.

To install a specific bioconductor package then do
```R
biocLite(PACKAGENAME)
```

Then to load it
```R
library(PACKAGENAME)
```

You have to do the installations only once, while you will have to load the packages everytime you start a fresh R Studio session.
If you create a project MYPRACTICA folder whose path is /MYDIR/ (in Windows it might look something like C: MYDIR/) in the R Studio console do

```R
setwd("/MYDIR/MYPRACTICA/")
```

In your /MYDIR/MYPRACTICA/ folder create a directory called "data". In a linux terminal this means:

```bash
cd /MYDIR/MYPRACTICA/
mkdir data
```

In /MYDIR/MYPRACTICA/data/ download and extract the [affymetrix files for the Col0 drought experiment](https://www.ncbi.nlm.nih.gov/geo/query/acc.cgi?acc=GSE11538). Since these are binay files, we will need R to look into them.

### Let's start!

Install the following packages plus the ones your installation might require (depends on R Studio versions etc)

```R
biocLite(c('affy', 'affyPLM', 'simpleaffy', 'limma', 'ath1121501.db', 'ath1121501cdf', 'ath1121501probe'))
```

Now load the .CEL files with the affy method ReadAffy

```R
library(affy)

flist = list.celfiles("data/", full.names=TRUE, pattern="GSM29")
col0wd = ReadAffy(filenames = flist)
```

Use R Studio interactive help to answer the following questions:

1. What type of object did the ReadAffy method return?
2. What methods are available for the col0wd object?

Now dig more into the affy functionalities. Open the affy package documentation and read it through
```R
vignette("affy")
```
