# Day 3 - analyze expression values

Microarrays studies are ofted performed to identify genes that are differentially expressed between different samples, e.g. different ecotypes under different treatments. Information on up-regulation (higher expression) or
down-regulation (lower expression) of genes can point to which genes are activated or deactivated in a different way with respect to a control sample.

The RMA method we used returns expression values X on a log2 scale. Since the mRNA amount is about $`2^{X}`$, each increase of 1 in X corresponds to a doubling of mRNA, which is a 2 fold change. This figure stolen from some slides by Dr Seungwoo Hwang shows why we want to use log2 scale

<img src="/uploads/f9f716fe59ffc6f5fc3be8abda6c8b1a/logscale.png" width="50%"></img>

## CDF file

So far we have only considered the [affymetrix .CEL files](http://www.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cel.html), containing the intensities values. Read on the affymetrix website the [description of the .CDF file](http://www.affymetrix.com/support/developer/powertools/changelog/gcos-agcc/cdf.html), which is needed to relate the probe sets on the chip to genes.

If you execute
```R
col0wd
```

You will se the cdf field value is ATH1-121501. The R package containing the relevant annotation is [ath1121501.db](https://bioconductor.org/packages/release/data/annotation/html/ath1121501.db.html). Load it and explore.

```R
library(ath1121501.db)
ath1121501.db
# Get the environment list
ath1121501()
# Select a probe id and have a look at some environment
x  = "245035_at"
ath1121501GENENAME[[x]]
ath1121501ENZYME[[x]]
ath1121501SYMBOL[[x]]
ath1121501ACCNUM[[x]]
```

## Differential expression analysis

One of your last assignment from yesterday was to familiarize with the bioconductor class ExpressionSet.
In the case of the col0wd data we have two sample groups (Watered and Drought) with three replicates each. The rma method returns the probe expression levels for each sample. In their paper, van Dijk et al. mention a list of genes of interest:

```R
# Genes of interest in van Dijk et al.
papergenes = c("AT4G34000", "AT5G51990", "AT4G27410", "AT5G52310", "AT5G52300", "AT2G46680", "AT3G61890")
# Obtain the probe id corresponding to papergenes
accnum2probe = as.data.frame(ath1121501ACCNUM)
findprobe = function(gid, df){
  return(df$probe_id[df$gene_id==gid])
}
paperprobes = sapply(papergenes, findprobe, accnum2probe)
```

  **Questions**
  1. Make sure you understand every line of the above code. Why convert to data.frame? Why define a function?
  2. Obtain the same object as paperprobes in a different way.



## Identify Up-regulated and Down-regulated genes

It is time now to use the limma package. You can load the documentation via

```R
limmaUsersGuide()
```

You can read as much as you want, for this course we will use only few methods in order to perform differential expression analysis. We want to compare for each probe the mean expression level in the Watered samples to the mean expression level in Drought samples: this is our design to be fitted by limma.  


```R
# phenoData(col0wd)$type is already a factor object
design = model.matrix(~ 0 + phenoData(col0wd)$type)
colnames(design) = c("Drought", "Watered")
dfit = lmFit(expval.col0wd, design)
# contrast matrix
cm = makeContrasts(Drought-Watered,levels=design)
dfit.con = contrasts.fit(dfit, cm)
```

Now we can call the eBayes method to perform moderated t-test on each probe. The result contains the t-statistics and the p-values as well as other variables.

```R
dfit.eb = eBayes(dfit.con)
dfit.eb$coefficients[1:10,]
dfit.eb$t[1:10,]
dfit.eb$p.value[1:10,]
```

To visualize the results we will use a Volcano plot. Here genes are arranged along biological and statistical significance. On the X-axis we have the log fold change between the sample groups (biological impact); the Y-axis we have the negative log of the p-value of the t-test (statistical evidence). We can then decide to highlight a different number of "most significant" of genes.

```R
volcanoplot(dfit.eb,coef=1,highlight=10)
```


  **Questions**
  1. Check the t-statistics and p-values for the genes mentioned in the van Dijk paper
  2. For the 10 genes highlighted in the Volcano plot, look for annotation information. Instead of browsing the internet  use ath1121501.db
  3. Do the same as point **1** on the genes you used (or will use) for qpcr
  
  ```R
  qpcrgenes = c("AT1G52690", "AT3G06420", "AT5G36910", "AT2G14750", "AT4G04610", "AT1G74090")
  ```

### Problems with Multiple Tests

Performing t-tests on many genes is an example of multiple testing, a procedure that can lead to many false positives. Consider the standard choice of the alpha level (type I error, the probability we have to reject the null hypothesis when it is true, or probability to have a false positive) to 0.05. If we have 1000 genes, and consider that 40 genes will be differentially expressed, out of the 960 remaining genes for which the null hypothesis should be true, we will reject 5% of them, i.e. 48 genes will be falsely considered differentially expressed. You can read more for example on the [wikipedia page](https://en.wikipedia.org/wiki/Multiple_comparisons_problem).

In R we can use the topTable method from limma to correct the p values.

```R
# correct for multiple testing
genetab = topTable(dfit.eb,coef=1,number=length(dfit.eb),adjust.method="BH")
siggenetab = genetab[genetab$adj.P.Val < 0.001, ]
upregtab = siggenetab[siggenetab$logFC > 1,]
downregtab = siggenetab[siggenetab$logFC < -1,]
dim(downregtab)
dim(upregtab)
```

  **Questions**
  1. Check the documentation of the topTable method. Do you understand what it does?
  2. Can you find the genes identified from the Volcano plot in the significant genes after correction?



### Want more?

Another approach in analysis of microarrays is to cluster genes according to their expression pattern to infer for example connections in response pathways. Search for relevant documentation and try! Be aware that if you call the heatmap method on all the probes your laptop might stall, consider plotting only the genes identified as significantly differentially expressed. Here attached the heatmap of the whole dataset.

[cluster.pdf](/uploads/09170d3ce7c202dddb04e17c0cd2ad33/cluster.pdf)
